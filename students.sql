drop database if exists students;
create database students;
use students;

create table classes(
id int primary key auto_increment,
nameClass varchar(255),
teacher varchar(255)
);
create table students(
id int primary key auto_increment,
fullName varchar(255) not null,
birthday date not null,
email varchar(255) not null,
classId int not null,
created_at DATETIME default now(),
FOREIGN KEY (classId) REFERENCES classes(id)
);
create table subjects(
id int primary key auto_increment,
nameSubject varchar(255)
);
create table semesters(
id int primary key auto_increment,
nameSemester varchar(255),
_year int 
);
create table points(
id int primary key auto_increment,
studentId int not null,
subjectId int not null,
semesterId int not null,
_point float ,
FOREIGN KEY (studentId) REFERENCES students(id),
FOREIGN KEY (subjectId) REFERENCES subjects(id),
FOREIGN KEY (semesterId) REFERENCES semesters(id)
);
insert into classes(nameClass,teacher)
values ("bootcamp01","hoàng"),
("bootcamp02","tính");


insert into students(fullName,birthday,email,classId)
values ("Minh Thện","1999-06-11","thien@gmail.com",1),
("Chí Trung",'1997-06-12',"trung@gmail.com",2),
("Đăng Khoa",'1995-07-11',"khoa@gmail.com",1),
("Lan Anh",'1992-07-11',"khoa@gmail.com",1);


insert into subjects(nameSubject)
values ("Java"),
("Javascript"),
("MySql"),
("NodeJs");

insert into semesters(nameSemester,_year)
values ("kỳ-1",2022),
("kỳ-2",2022),
("kỳ-1",2021),
("kỳ-2",2021);

insert into points(studentId,subjectId,semesterId,_point)
values (1,2,1,7),
(1,3,1,8),
(1,1,1,6),
(1,1,2,4),
(1,2,2,5),
(1,3,2,9),


(2,1,2,9),
(2,2,2,10),
(2,3,2,9),
(2,1,3,9),
(2,2,3,10),
(2,3,3,9),
(2,1,4,8),
(2,2,4,9),
(2,3,4,9),

(3,1,1,5),
(3,2,1,8),
(3,3,1,9),
(3,1,2,9),
(3,2,2,10),
(3,3,2,9),

(4,1,1,3),
(4,2,1,4),
(4,3,1,5),
(4,1,2,3),
(4,2,2,2),
(4,3,2,4);
insert into points(studentId,subjectId,semesterId)
values
(2,1,1),
(2,2,1),
(2,3,1);
