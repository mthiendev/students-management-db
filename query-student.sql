use students;
 --  tìm điểm
select students.fullName ,subjects.nameSubject, points._point,semesters.nameSemester , semesters._year from points
left join students
on students.id = points.studentId
left join subjects
on subjects.id = points.subjectId
left join semesters
on semesters.id = points.semesterId
 where students.id=2 and subjects.id =2 and semesters.nameSemester="kỳ-1" and semesters._year = 2022; 
 
 --  tìm sinh viên có điểm trung bình cao nhất của học kỳ 1-2022
 select students.fullName,semesters.nameSemester,semesters._year,avg(points._point) as "average point" from points
 left join students
on students.id = points.studentId
left join subjects
on subjects.id = points.subjectId
left join semesters
on semesters.id = points.semesterId
where semesters.nameSemester="kỳ-1" and semesters._year =2022
group by students.id
ORDER BY avg(points._point) DESC
limit 1;

-- tìm sinh viên điểm trung bình bé hơn bằng 5
select students.fullName,semesters.nameSemester,semesters._year,avg(points._point) as "average point" from points
 left join students
on students.id = points.studentId
left join subjects
on subjects.id = points.subjectId
left join semesters
on semesters.id = points.semesterId
group by students.id
having avg(points._point) <=5;

-- lớp học có nhiều học viên nhất
select nameClass,teacher,count(classId) as mountClass from students
left join classes 
on students.classId= classes.id
group by classId
order by count(classId) desc
limit 1;
-- tìm sv đă hoàn thành những học kỳ học nào
select distinct semesters.id, semesters.nameSemester, semesters._year  from points
left join students
on students.id = points.studentId
left join semesters 
on semesters.id =points.semesterId
where students.id =2 and points._point is not null;

-- tìm sv có tên kí tự dài nhất
select *, length(fullName) as LengthOfFullName from students
order by length(fullName) desc
limit 1;

-- tìm sv có tên kí tự ngắn nhất
select *, length(fullName) as LengthOfFullName from students
order by length(fullName) asc
limit 1








